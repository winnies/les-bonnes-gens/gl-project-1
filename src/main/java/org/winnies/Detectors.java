package org.winnies;

public interface Detectors {
    interface Listener {
        void cageDetected();
    }

    void addListener(Listener listener);
    void removeListener(Listener listener);
}
