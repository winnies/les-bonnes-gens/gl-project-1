package org.winnies;

public interface GUI {
    public interface Listener {
        enum WantedDirection {
            Upstairs, Downstairs,
        }

        void emergencyStopRequested();
        void indoorRequestHappened(int floor);
        void outdoorRequestHappened(int floor, WantedDirection wantedDirection);
    }

    void setEmergencyButtonState(boolean enabled);
    void setIndoorButtonState(int floor, boolean enabled);
    void setOutdoorButtonState(int floor, Listener.WantedDirection wantedDirection, boolean enabled);
    void setDoorsOpened(boolean opened);
    void addListener(Listener listener);
    void removeListener(Listener listener);
}
