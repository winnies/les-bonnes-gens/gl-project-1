package org.winnies.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.winnies.GUI;

public class ElevatorInsideInterfaceFragmentController {
    PrimaryController primaryController;
    private int floor;

    @FXML
    Button floorSelectButton;

    public void init(PrimaryController primaryController, int floor) {
        this.primaryController = primaryController;
        this.floor = floor;
        floorSelectButton.setText(Integer.toString(floor));
    }

    @FXML
    public void floorSelection(ActionEvent e) {
        for (GUI.Listener listener: primaryController.listeners) {
            listener.indoorRequestHappened(floor);
        }
    }
}
