package org.winnies.gui;

import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import org.winnies.GUI;

public class PrimaryController implements GUI {
    private ArrayList<ElevatorOutsideInterfaceFragmentController> outsideControllers = new ArrayList<>();
    private ArrayList<ElevatorInsideInterfaceFragmentController> insideControllers = new ArrayList<>();
    public ArrayList<Listener> listeners = new ArrayList<>();
    private static final PseudoClass SELECTED_PSEUDO_CLASS = PseudoClass.getPseudoClass("selected");

    @FXML
    public Slider slider;

    @FXML
    private VBox elevatorOutsideCommand;

    @FXML
    private GridPane elevatorInsideCommand;

    @FXML
    private Button stopButton;

    @FXML
    public Label stateLabel;

    @Override
    public void setEmergencyButtonState(boolean enabled) {
        Platform.runLater(() -> stopButton.pseudoClassStateChanged(SELECTED_PSEUDO_CLASS, enabled));
    }

    @Override
    public void setIndoorButtonState(int floor, boolean enabled) {
        Platform.runLater(() -> insideControllers.get(floor).floorSelectButton.pseudoClassStateChanged(SELECTED_PSEUDO_CLASS, enabled));
    }

    @Override
    public void setOutdoorButtonState(int floor, Listener.WantedDirection wantedDirection, boolean enabled) {
        Platform.runLater(() -> {
            if (wantedDirection == Listener.WantedDirection.Upstairs) {
                outsideControllers.get(floor).up.pseudoClassStateChanged(SELECTED_PSEUDO_CLASS, enabled);
            } else if (wantedDirection == Listener.WantedDirection.Downstairs) {
                outsideControllers.get(floor).down.pseudoClassStateChanged(SELECTED_PSEUDO_CLASS, enabled);
            }
        });
    }

    @Override
    public void setDoorsOpened(boolean opened) {
        Platform.runLater(() -> {
            slider.pseudoClassStateChanged(SELECTED_PSEUDO_CLASS, opened);
        });
    }

    @Override
    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    public void emergencyButtonPress(ActionEvent e) {
        for (GUI.Listener listener: listeners) {
            listener.emergencyStopRequested();
        }
    }

    public void setFloorNumber(int floorNumber){
        slider.setMax(floorNumber - 1);

        stateLabel.setText("= 0");

        Parent root = null;

        outsideControllers = new ArrayList<>(floorNumber);
        for (int i = 0; i < floorNumber; ++i)
            outsideControllers.add(null);

        // Creating the outside buttons interface.
        // Separating the first and last to remove up button for topmost button
        // And down button for ground floor.
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("elevatorOutsideInterfaceFragment.fxml"));
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException("Could not load interface fragment.");
        }
        ElevatorOutsideInterfaceFragmentController outsideFragmentController = fxmlLoader.getController();
        outsideFragmentController.init(this, floorNumber  - 1);
        outsideControllers.set(floorNumber  - 1, outsideFragmentController);
        outsideFragmentController.buttonVBox.getChildren().remove(outsideFragmentController.up);
        elevatorOutsideCommand.getChildren().add(root);

        elevatorOutsideCommand.getChildren().add(new Separator());

        // Creating the buttons between the first and the last
        for (int i = floorNumber  - 2; i >= 1; --i) {
            fxmlLoader = new FXMLLoader(getClass().getResource("elevatorOutsideInterfaceFragment.fxml"));
            try {
                root = fxmlLoader.load();
            } catch (IOException e) {
                throw new RuntimeException("Could not load interface fragment.");
            }
            outsideFragmentController = fxmlLoader.getController();
            outsideFragmentController.init(this, i);
            outsideControllers.set(i, outsideFragmentController);
            elevatorOutsideCommand.getChildren().add(root);

            elevatorOutsideCommand.getChildren().add(new Separator());
        }

        fxmlLoader = new FXMLLoader(getClass().getResource("elevatorOutsideInterfaceFragment.fxml"));
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException("Could not load interface fragment.");
        }
        outsideFragmentController = fxmlLoader.getController();
        outsideFragmentController.init(this, 0);
        outsideControllers.set(0, outsideFragmentController);
        elevatorOutsideCommand.getChildren().add(root);
        outsideFragmentController.buttonVBox.getChildren().remove(outsideFragmentController.down);

        // Setting the inside buttons gridpane constraints
        for (int i = 0; i < floorNumber / 3; i++) {
            RowConstraints rowConstraints = new RowConstraints();
            // Max height
            rowConstraints.setPrefHeight(30.);
            rowConstraints.setMaxHeight(1.7976931348623157E308);
            rowConstraints.setVgrow(Priority.SOMETIMES);
            elevatorInsideCommand.getRowConstraints().add(rowConstraints);
        }

        // Creating the buttons of the inside interface
        for (int i = 0; i < floorNumber; i++) {
            fxmlLoader = new FXMLLoader(getClass().getResource("elevatorInsideInterfaceFragment.fxml"));
            try {
                root = fxmlLoader.load();
            } catch (IOException e) {
                throw new RuntimeException("Could not load outside interface fragment.");
            }
            ElevatorInsideInterfaceFragmentController insideFragmentController = fxmlLoader.getController();
            insideFragmentController.init(this, i);
            insideControllers.add(insideFragmentController);
            GridPane.setValignment(root, VPos.CENTER);
            GridPane.setHalignment(root, HPos.CENTER);
            elevatorInsideCommand.add(root, i % 3, floorNumber - i / 3);
        }
    }
}
