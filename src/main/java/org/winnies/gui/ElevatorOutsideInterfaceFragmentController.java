package org.winnies.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import org.winnies.GUI;

public class ElevatorOutsideInterfaceFragmentController {
    PrimaryController primaryController;
    private int floor;

    @FXML
    public VBox buttonVBox;

    @FXML
    private Label floorLabel;

    @FXML
    public Button up;

    @FXML
    public Button down;

    public void init(PrimaryController primaryController, int floor) {
        this.primaryController = primaryController;
        this.floor = floor;
        floorLabel.setText(Integer.toString(floor));
    }

    @FXML
    public void goUp(ActionEvent event) {
        for (GUI.Listener listener: primaryController.listeners) {
            listener.outdoorRequestHappened(floor, GUI.Listener.WantedDirection.Upstairs);
        }
    }

    @FXML
    public void goDown(ActionEvent event) {
        for (GUI.Listener listener: primaryController.listeners) {
            listener.outdoorRequestHappened(floor, GUI.Listener.WantedDirection.Downstairs);
        }
    }
}
