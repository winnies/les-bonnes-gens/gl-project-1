package org.winnies.gui;

import javafx.fxml.FXML;
import javafx.scene.control.Spinner;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import org.winnies.App;

public class FloorNumberQueryController {
    final int maxFloors = 16;
    App app;

    @FXML
    Pane spinnerPane;

    Spinner<Integer> floorNumberSpinner;

    public void init(App app) {
        this.app = app;
        // Creating the spinner here because Scenebuilder doesn't allow for
        // specific actions bound to keys like "enter".
        // Plus, it doesn't specify if it's an integer or double or whatever.
        floorNumberSpinner = new Spinner<>(2, maxFloors, 6);
        floorNumberSpinner.setId("floorNumberSpinner");
        floorNumberSpinner.setOnKeyReleased(event -> {
            // Setting enter key action
            if (event.getCode() == KeyCode.ENTER){
                submitFloorNumber();
            }
        });
        spinnerPane.getChildren().add(floorNumberSpinner);
    }

    @FXML
    public void submitFloorNumber(){
        int input = floorNumberSpinner.getValue();
        app.startElevator(input);
    }
}
