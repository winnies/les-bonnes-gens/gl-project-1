package org.winnies;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.winnies.gui.FloorNumberQueryController;
import org.winnies.gui.PrimaryController;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * JavaFX App
 */
public class App extends Application {
    public static Stage stage;
    private static Scene scene;
    private final static int floorNumber = 16;
    private static FakeEngine engine = new FakeEngine(floorNumber);
    private static Scheduler scheduler;

    @Override
    public void start(Stage stage) throws IOException {
        App.stage = stage;
        stage.setTitle("El elevator");

        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("gui/floor_number_query.fxml")
        );
        scene = new Scene(loader.load());
        FloorNumberQueryController controller = loader.getController();
        controller.init(this);
        stage.setScene(scene);
        stage.sizeToScene();

        stage.setMinWidth(stage.getWidth());
        stage.setMinHeight(stage.getHeight());
        stage.show();
    }

    public void startElevator(int floorNumber) {
        stage.setTitle("El elevator");
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("gui/primary.fxml")
        );

        try {
            scene = new Scene(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(scene);

        PrimaryController controller = loader.getController();
        controller.setFloorNumber(floorNumber);

        stage.sizeToScene();

        final AtomicReference<FakeEngine.State> latestState = new AtomicReference<>();

        engine.addListener(state -> {
            if (latestState.getAndSet(state) == null) {
                Platform.runLater(() -> {
                    controller.slider.adjustValue(latestState.getAndSet(null).floor);
                    String strMove = null;
                    switch (state.moveState) {
                        case Stopped:
                            strMove = "=";
                            break;
                        case GoingUp:
                            strMove = "⇑";
                            break;
                        case GoingDown:
                            strMove = "⇓";
                            break;
                    }
                    int d = (int)Math.ceil(state.floor);
                    int u = (int)Math.floor(state.floor);
                    if (d != u) {
                        controller.stateLabel.setText(strMove + " " + u + "-" + d);
                    } else {
                        controller.stateLabel.setText(strMove + " " + d);
                    }
                });
            }
        });

        scheduler = new Scheduler(engine, engine, floorNumber, controller);

        stage.show();
        stage.setMinWidth(stage.getWidth());
        stage.setMinHeight(stage.getHeight());
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        engine.startRunning();
        launch();
        scheduler.shutdown();
        engine.stopRunning();
    }

}