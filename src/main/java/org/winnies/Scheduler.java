package org.winnies;

import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Scheduler implements GUI.Listener, Detectors.Listener {

    private enum Direction {
        UP,
        DOWN,
        NONE
    }

    private boolean emergencyStop;
    private Engine engine;
    private Direction elevatorDirection;
    private Direction[] requestTable;
    private GUI gui;

    private int floorsNumber;
    private int floor;
    private boolean doorsOpened = false;
    private long waitDoorTimeMili = 2000;
    private boolean wasEmergencyStop = false;

    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    public Scheduler(Engine engine, Detectors detector, int floorsNumber, GUI gui) {
        this.engine = engine;
        this.floorsNumber = floorsNumber;
        this.floor = 0;
        this.elevatorDirection = Direction.NONE;
        this.gui = gui;

        this.requestTable = new Direction[floorsNumber];
        setDefaultRequests();

        gui.addListener(this);
        detector.addListener(this);
    }


    //Interaction avec Scheduler
    @Override
    public void cageDetected() {
        executor.execute(() -> _cageDetected());
    }

    public void _cageDetected() {
        if (elevatorDirection == Direction.UP && floor < floorsNumber)
            floor++;
        else if (elevatorDirection == Direction.DOWN && floor > 0)
            floor--;

        checkOpenDoor();
        directionUpdate();
    }

    @Override
    public void indoorRequestHappened(int floor) {
        executor.execute(() -> _indoorRequestHappened(floor));
    }

    private void _indoorRequestHappened(int floor) {
        if (emergencyStop || !floorIsValid(floor))
            return;

        System.out.println("Pressed :" + elevatorDirection);

        if (floor > this.floor) {
            requestTable[floor] = Direction.UP;
            gui.setIndoorButtonState(floor, true);
        } else if (floor < this.floor) {
            requestTable[floor] = Direction.DOWN;
            gui.setIndoorButtonState(floor, true);
        } else if (floor == this.floor)
        {
            requestTable[floor] = Direction.UP;
            gui.setIndoorButtonState(floor, true);
        }

        if(elevatorDirection == Direction.NONE)
        {
            checkOpenDoor();
            directionUpdate();
        }
        else
            directionUpdate();

        System.out.println("Released :" + elevatorDirection);
    }

    @Override
    public void outdoorRequestHappened(int floor, WantedDirection wantedDirection) {
        executor.execute(() -> _outdoorRequestHappened(floor, wantedDirection));
    }

    private void _outdoorRequestHappened(int floor, WantedDirection wantedDirection) {
        if (emergencyStop || !floorIsValid(floor))
            return;

        System.out.println(elevatorDirection);

        if (wantedDirection == WantedDirection.Upstairs && requestCanBeOveriten(floor)) {
            requestTable[floor] = Direction.UP;
            gui.setOutdoorButtonState(floor, wantedDirection, true);
        } else if (wantedDirection == WantedDirection.Downstairs && requestCanBeOveriten(floor)) {
            requestTable[floor] = Direction.DOWN;
            gui.setOutdoorButtonState(floor, wantedDirection, true);
        }
        else if (!floorNotRequested(floor))
            gui.setOutdoorButtonState(floor, wantedDirection, true);


        if(elevatorDirection == Direction.NONE)
        {
            checkOpenDoor();
            directionUpdate();
        }
        else
            directionUpdate();

    }


    public void checkOpenDoor()
    {
        if(stopThisFloor())
        {
            openDoor();
        }
        else if(!missionUp() && !missionDown())
        {
            stop();
        }
    }

    private void directionUpdate()
    {
        if(doorsOpened)
            return;

        switch (elevatorDirection) {
            case UP:
                if (floorIsValid(floor) && missionUp())
                {
                    up();
                    if(nextFloorStopUp())
                        engine.stopNextFloor();
                }
                else if (floorIsValid(floor) && missionDown())
                {
                    down();
                    if(nextFloorStopDown())
                        engine.stopNextFloor();
                }
                else
                    elevatorDirection = Direction.NONE;
                break;
            case DOWN:
                if (floorIsValid(floor) && missionDown())
                {
                    down();
                    if(nextFloorStopDown())
                        engine.stopNextFloor();
                }
                else if (floorIsValid(floor) && missionUp())
                {
                    up();
                    if(nextFloorStopUp())
                        engine.stopNextFloor();
                }
                else
                    elevatorDirection = Direction.NONE;
                break;
            case NONE:
                if (floorIsValid(floor) && missionUp())
                {
                    up();
                    if(nextFloorStopUp())
                        engine.stopNextFloor();
                }
                else if (floorIsValid(floor) && missionDown())
                {
                    down();
                    if(nextFloorStopDown())
                        engine.stopNextFloor();
                }
                break;
        }

    }

    private boolean stopThisFloor()
    {
        switch (elevatorDirection)
        {
            case UP:
                if (requestTable[floor] == Direction.UP) {
                    return true;
                }
                else if(requestTable[floor] != Direction.NONE && !missionUp())
                {
                    return true;
                }
                break;
            case DOWN:
                if (requestTable[floor] == Direction.DOWN) {
                    return true;
                }
                else if(requestTable[floor] != Direction.NONE && !missionDown())
                {
                    return true;
                }
                break;
            case NONE:
                if (requestTable[floor] != Direction.NONE) {
                    return true;
                }
                break;
        }
        return false;
    }

    //Action
    private void openDoor() {
        if(doorsOpened)
            return;

        doorsOpened = true;
        gui.setIndoorButtonState(floor, false);
        gui.setOutdoorButtonState(floor, WantedDirection.Upstairs, false);
        gui.setOutdoorButtonState(floor, WantedDirection.Downstairs, false);
        requestTable[floor] = Direction.NONE;
        gui.setDoorsOpened(true);
        System.out.println("PORTE OUVRTE");

        engine.stop();
        executor.schedule(() -> {
            gui.setIndoorButtonState(floor, false);
            gui.setOutdoorButtonState(floor, WantedDirection.Upstairs, false);
            gui.setOutdoorButtonState(floor, WantedDirection.Downstairs, false);
            requestTable[floor] = Direction.NONE;
            doorsOpened = false;
            gui.setDoorsOpened(false);
            System.out.println("PORTE FERME");
            directionUpdate();
        }, waitDoorTimeMili, TimeUnit.MILLISECONDS);

    }

    private void up() {
        elevatorDirection = Direction.UP;
        engine.goUp();
    }

    private void down() {
        elevatorDirection = Direction.DOWN;
        engine.goDown();
    }

    private void stop() {
        elevatorDirection = Direction.NONE;
        engine.stop();
    }

    private boolean floorIsValid(int floor) {
        return (floor < floorsNumber && floor >= 0);
    }

    private boolean requestCanBeOveriten(int floor)
    {
        System.out.println(this.requestTable[floor]);
        System.out.println(elevatorDirection);
        System.out.println(this.requestTable[floor] != Direction.NONE);
        System.out.println(this.requestTable[floor] != elevatorDirection || this.requestTable[floor] == Direction.NONE);
        return floorIsValid(floor) && (this.requestTable[floor] != elevatorDirection || this.requestTable[floor] == Direction.NONE) ;
    }

    @Override
    public void emergencyStopRequested() {
        executor.execute(() -> _emergencyStopRequested());
    }

    private void _emergencyStopRequested() {
        if (emergencyStop) {
            emergencyStop = false;
            gui.setEmergencyButtonState(false);
            wasEmergencyStop = true;

            System.out.println("STOP eleDir : " + elevatorDirection);

            if(elevatorDirection == Direction.UP)
            {
                up();
                engine.stopNextFloor();
                requestTable[floor+1] = Direction.UP;
            }
            else if(elevatorDirection == Direction.DOWN)
            {
                down();
                engine.stopNextFloor();
                requestTable[floor-1] = Direction.DOWN;
            }

            return;
        }

        emergencyStop = true;
        setDefaultRequests();
        gui.setEmergencyButtonState(true);
        engine.stop();
    }

    // Boite à outils
    private boolean nextFloorStopUp()
    {
        if(nextFloorHaveAGoodDirection(floor+1))
        {
            return true;
        }
        else if(!floorNotRequested(floor+1) && !missionUp(floor+1))
        {
            return true;
        }
        return false;
    }

    private boolean nextFloorStopDown()
    {
        if(nextFloorHaveAGoodDirection(floor-1))
        {
            return true;
        }
        else if(!floorNotRequested(floor-1) && !missionDown(floor-1))
        {
            return true;
        }
        return false;
    }

    private boolean missionUp() {
        if(floorIsValid(floor+1))
        {
            for (int i = floor+1; i < floorsNumber; i++) {
                if (!floorNotRequested(i))
                    return true;
            }
        }
        return false;
    }

    private boolean missionDown() {
        if(floorIsValid(floor-1))
        {
            for (int i = floor-1; i >= 0; i--) {
                if (!floorNotRequested(i))
                    return true;
            }
        }
        return false;
    }

    private boolean missionUp(int floor) {
        if(floorIsValid(floor+1))
        {
            for (int i = floor+1; i < floorsNumber; i++) {
                if (!floorNotRequested(i))
                    return true;
            }
        }
        return false;
    }

    private boolean missionDown(int floor) {
        if(floorIsValid(floor-1))
        {
            for (int i = floor-1; i >= 0; i--) {
                if (!floorNotRequested(i))
                    return true;
            }
        }
        return false;
    }

    private boolean floorNotRequested(int floor) {
        return floorIsValid(floor) && this.requestTable[floor] == Direction.NONE;
    }

    private boolean nextFloorHaveAGoodDirection(int floor) {
        return floorIsValid(floor) && this.requestTable[floor] == this.elevatorDirection;
    }

    private void setDefaultRequests() {
        Arrays.fill(requestTable, Direction.NONE);
        for (int floor = 0; floor < floorsNumber; ++floor) {
            gui.setIndoorButtonState(floor, false);
            gui.setOutdoorButtonState(floor, WantedDirection.Upstairs, false);
            gui.setOutdoorButtonState(floor, WantedDirection.Downstairs, false);
        }
    }

    public void shutdown() {
        executor.shutdown();
    }

}
