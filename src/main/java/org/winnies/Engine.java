package org.winnies;

public interface Engine {
    void goUp();
    void goDown();
    void stop();
    void stopNextFloor();
}
