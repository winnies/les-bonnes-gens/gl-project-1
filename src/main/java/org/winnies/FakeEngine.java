package org.winnies;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;

public class FakeEngine implements Engine, Detectors {
    private final float ACCELERATION = 0.000001f; // 1 / stage / ms
    private final float DECELERATION = 0.0000005f; // 1 / stage / ms
    private final float ACCELERATION_WANTED_SPEED = 0.001f; // stage / ms
    private final float DECELERATION_WANTED_SPEED = 0.0002f; // stage / ms

    private int floorCount;

    private volatile boolean running = false;

    private final Object signal = new Object();
    private final Lock updateLock = new ReentrantLock();
    private volatile boolean updated = false;
    private volatile MoveState moveState = null;
    private volatile Boolean stopNextFloor = null;
    private volatile Float wantedSpeed = null;

    private final Thread updateThread = new Thread(
            () -> {
                final State state = new State();
                float currentSpeed = 0;

                long lastTime = System.currentTimeMillis();
                boolean wasNearAFloor = state.isNearAFloor();

                while (running) {
                    long currentTime = System.currentTimeMillis();
                    long timeElapsed = currentTime - lastTime;
                    lastTime = currentTime;

                    if (updated && updateLock.tryLock()) {
                        if (moveState != null) {
                            state.moveState = moveState;
                            moveState = null;

                            if (state.moveState == MoveState.Stopped) {
                                currentSpeed = 0;
                            }
                        }

                        if (stopNextFloor != null) {
                            state.stopNextFloor = stopNextFloor;
                            stopNextFloor = null;
                        }

                        if (wantedSpeed != null) {
                            state.wantedSpeed = wantedSpeed;
                            wantedSpeed = null;
                        }

                        updated = false;
                        updateLock.unlock();
                    }

                    if (currentSpeed < state.wantedSpeed) {
                        currentSpeed = Math.min(state.wantedSpeed, currentSpeed + ACCELERATION * timeElapsed);
                    } else if (currentSpeed > state.wantedSpeed) {
                        currentSpeed = Math.max(state.wantedSpeed, currentSpeed - DECELERATION * timeElapsed);
                    }

                    float remainingMove = currentSpeed * timeElapsed;
                    while (state.canMove() && remainingMove > 0.f) {
                        float move = Math.min(State.nearFloor, remainingMove);
                        remainingMove -= move;

                        switch (state.moveState) {
                            case GoingUp:
                                state.floor += move;
                                break;
                            case GoingDown:
                                state.floor -= move;
                                break;
                        }

                        boolean isNearAFloor = state.isNearAFloor();
                        if ((wasNearAFloor != isNearAFloor) && isNearAFloor) {
                            if (state.stopNextFloor || !state.canMove()) {
                                state.stopNextFloor = false;
                                state.floor = Math.round(state.floor);
                                state.moveState = MoveState.Stopped;
                                currentSpeed = 0;
                            }

                            cageDetected();
                        }

                        wasNearAFloor = isNearAFloor;
                    }

                    stateChanged(state);

                    try {
                        if (state.moveState == MoveState.Stopped && !updated)
                            synchronized (signal) {
                                signal.wait();
                                lastTime = System.currentTimeMillis();
                            }
                        else
                            sleep(16);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
    );

    final List<Detectors.Listener> detectorsListeners = new ArrayList<>();

    @Override
    public void addListener(Detectors.Listener listener) {
        detectorsListeners.add(listener);
    }

    @Override
    public void removeListener(Detectors.Listener listener) {
        detectorsListeners.remove(listener);
    }

    private void cageDetected() {
        for (Detectors.Listener listener : detectorsListeners) {
            listener.cageDetected();
        }
    }

    public enum MoveState {
        Stopped, GoingUp, GoingDown
    }

    public class State implements Cloneable {
        static final float nearFloor = 0.01f;
        MoveState moveState = MoveState.Stopped;
        boolean stopNextFloor = false;
        float floor = 0;
        float wantedSpeed = 0;

        public boolean isNearAFloor() {
            return 1.f - nearFloor < floor % 1 || floor % 1 < nearFloor;
        }

        public int getExactFloor() {
            if (isNearAFloor())
                return Math.round(floor);
            else
                return -1;
        }

        public boolean canMove() {
            return canMoveUp() || canMoveDown();
        }

        public boolean canMoveUp() {
            return (moveState == MoveState.GoingUp) && floor < floorCount;
        }

        public boolean canMoveDown() {
            return (moveState == MoveState.GoingDown) && floor > 0;
        }

        @Override
        public State clone() {
            try {
                return (State) super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public interface Listener {
        void stateChanged(State state);
    }

    private ArrayList<Listener> listeners = new ArrayList<>();

    public FakeEngine(int floorCount) {
        this.floorCount = floorCount;
    }

    public int getFloorCount() {
        return floorCount;
    }

    public void startRunning() {
        if (running)
            return;

        running = true;
        updateThread.start();
    }

    public void stopRunning() {
        if (!running)
            return;

        running = false;
        synchronized (signal) {
            signal.notifyAll();
        }
        try {
            updateThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void goUp() {
        updateLock.lock();
        updated = true;
        moveState = MoveState.GoingUp;
        wantedSpeed = ACCELERATION_WANTED_SPEED;
        updateLock.unlock();

        synchronized (signal) {
            signal.notifyAll();
        }
    }

    @Override
    public void goDown() {
        updateLock.lock();
        updated = true;
        moveState = MoveState.GoingDown;
        wantedSpeed = ACCELERATION_WANTED_SPEED;
        updateLock.unlock();

        synchronized (signal) {
            signal.notifyAll();
        }
    }

    @Override
    public void stop() {
        updateLock.lock();
        updated = true;
        moveState = MoveState.Stopped;
        stopNextFloor = false;
        updateLock.unlock();

        synchronized (signal) {
            signal.notifyAll();
        }
    }

    @Override
    public void stopNextFloor() {
        updateLock.lock();
        updated = true;
        stopNextFloor = true;
        wantedSpeed = DECELERATION_WANTED_SPEED;
        updateLock.unlock();

        synchronized (signal) {
            signal.notifyAll();
        }
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    private void stateChanged(State state) {
        State copy = state.clone();
        for (Listener listener : listeners) {
            listener.stateChanged(copy);
        }
    }
}
