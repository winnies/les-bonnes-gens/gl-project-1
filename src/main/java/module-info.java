module org.winnies {
    requires javafx.controls;
    requires javafx.fxml;

    opens org.winnies to javafx.fxml;
    opens org.winnies.gui to javafx.fxml;
    exports org.winnies;
}