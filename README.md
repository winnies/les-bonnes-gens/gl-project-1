----------------------------------------------------------------------------------
                  ______ _        _                 _             
                 |  ____| |      | |               | |            
                 | |__  | |   ___| | _____   ____ _| |_ ___  _ __ 
                 |  __| | |  / _ \ |/ _ \ \ / / _` | __/ _ \| '__|
                 | |____| | |  __/ |  __/\ V / (_| | || (_) | |   
                 |______|_|  \___|_|\___| \_/ \__,_|\__\___/|_|                    
----------------------------------------------------------------------------------

--------------------------------
PROJECT GROUP
--------------------------------
	Doiteau Laurent
	Escales Loïc
	Galland Thomas
	Gillet Pierre
	Wolfger Killian
	
--------------------------------
 PROJECT
--------------------------------
	2019 University project.
	Application simulating the managment and 
	the use of an elevator in a building with 
	a pickable number of floors.

--------------------------------
RUN
--------------------------------
	To run the project, use the following command line according
	to your distrib:

	On Linux:
		$ java -jar el_Elevator.jar
	On Windows:
		double click on the el_Elevator.jar file
	

    Note: some warnings and build errors may show up, they are due to 
    the javaFX API version we are using and in which javaFX API version
    some documents are wrotten.

--------------------------------
COPYRIGHT
--------------------------------
© Doiteau Laurent, Escales Loïc, Galland Thomas, Gillet Pierre, Wolfger Killian, 2019, all rights reserved.